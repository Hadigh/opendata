<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="style.css" />
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>


<!-- JQuery DataTable -->
    <script type="text/javascript" src="/~hghaddar/opendata/jquery/jquery-3.4.1.js"></script>
    <!-- DataTable CSS -->
    <link rel="stylesheet" type="text/css" href="datatables.min.css"/>
    
    <!-- DataTable JavaScript -->
    <script type="text/javascript" src="datatables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#tab').DataTable();
        } );
    </script>


        <title>Recherche sur l'enseignement supérieur</title>
    </head>

     <body>



<?php

$url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=200&sort=-rentree_lib&facet=etablissement_type_lib&facet=diplome_lib&facet=discipline_lib&facet=com_etab_lib&facet=libelle_intitule_1&refine.rentree_lib=2017-18";

$key = "&apikey=0be72d7440fa9d41312deecd105b2408fc41c7ecb06b059bfaaf6a5e";


$url2 = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&rows=-1";




if($_POST['recherchetypediplome'] != ''){
	$filtre = "&refine.diplome_lib=".$_POST['recherchetypediplome'];
}else{
    $filtre = "";
}


if($_POST['recherchenomdiplome'] != ''){
    $filtre2 = "&refine.libelle_intitule_1=".$_POST['recherchenomdiplome'];
}else{
    $filtre2 = "";
}




if($_POST['rechercheetablissement'] != ''){
    $filtre3 = "&refine.etablissement_lib=".$_POST['rechercheetablissement'];
}else{
    $filtre3 = "";
}

if($_POST['rechercheville'] != ''){
    $filtre4 = "&refine.com_etab_lib=".$_POST['rechercheville'];
}else{
    $filtre4 = "";
}


$data = file_get_contents($url.$filtre.$filtre2.$filtre3.$filtre4.$key);
/*
$data = file_get_contents("base.json"); 
*/
$schools = json_decode($data);
$school2 = json_decode($data,true);


//bdd 2 pour les marqueurs


$data2 = file_get_contents($url2.$key);




$etablissement = json_decode($data2,true);


if (empty($schools)) exit('ERROR : EMPTY');

?>







<ul>
    <li><a href="#1">Accueil</a></li>
    <li><a href="#2">Etablissements Supérieurs</a></li>
    <li><a href="#3">Zone Géographique</a></li>
    <li><a href="#4">Formations</a></li>
    <li><a href="#5">Se connecter</a></li>
    <li><a href="#6">Options</a></li>
</ul>
            




                
  

            
                 
              


<form action="index.php" method="post">

 <select name="recherchetypediplome" id='recherchetypediplome'>

 <option value=''>-- Type de diplome--</option>
 <option value='Licence'>Licence</option>
 <option value='Doctorat'>Doctorat</option>
 <option value="Diplômes d'établissement">Diplômes d'établissement</option>
 <option value="Formations d'ingénieurs">Formations d'ingénieurs</option>
 <option value="Autres formations de santé">Autres formations de santé</option>
 <option value="Licence professionnelle">Licence professionnelle</option>
  <option value="Diplôme universitaire de technologie">Diplôme universitaire de technologie</option>

<option value="HDR">HDR</option>

 </select>




 <select name="rechercheville" id='rechercheville'>

 <option value=''>-- Ville--</option>
 <option value="Marseille 7e">Marseille 7e</option>
 <option value="Grenoble">Grenoble</option>
 <option value="Rennes">Rennes</option>
 <option value="Toulouse">Toulouse</option>
 <option value="Besançon">Besançon</option>
 <option value="Dijon">Dijon</option>
 <option value="Orléans">Orléans</option>
 <option value="Brest">Brest</option>
 <option value="Caen">Caen</option>

 <option value="La Rochelle">La Rochelle</option>
 <option value="Limoges">Limoges</option>
 <option value="Tours">Tours</option>
  <option value="Montpellier">Montpellier</option>
   <option value="Troyes">Troyes</option>
    <option value="Saint-Étienne">Saint-Étienne</option>
     <option value="Nice">Nice</option>
      <option value="Créteil">Créteil</option>
 <option value="Saint-Denis">Saint-Denis</option>

 </select>











 <select name="recherchenomdiplome" id='recherchenomdiplome'>

 <option value=''>-- Nom du diplome--</option>
 <option value='CHIMIE'>Chimie</option>
 <option value="HISTOIRE">Histoire</option>
<option value="DROIT">DROIT</option>
<option value="SCIENCES ET TECHNOLOGIES : INFORMATIQUE">SCIENCES ET TECHNOLOGIES : INFORMATIQUE</option>
<option value="DROIT, ECONOMIE, GESTION : ETUDES POLITIQUES">DROIT, ECONOMIE, GESTION : ETUDES POLITIQUES</option>
<option value="SCIENCES ET TECHNOLOGIES : STAPS">SCIENCES ET TECHNOLOGIES : STAPS</option>
<option value="SCIENCES ET TECHNOLOGIES : OCEANOGRAPHIE">SCIENCES ET TECHNOLOGIES : OCEANOGRAPHIE</option>

<option value="ANESTHESIE REANIMATION">ANESTHESIE REANIMATION</option>
<option value="SCIENCES ET TECHNOLOGIES : OCEANOGRAPHIE">SCIENCES ET TECHNOLOGIES : OCEANOGRAPHIE</option>
<option value="HEPATO-GASTRO-ENTEROLOGIE">HEPATO-GASTRO-ENTEROLOGIE</option>

<option value="PEDIATRIE">PEDIATRIE</option>
<option value="HEMATOLOGIE">HEMATOLOGIE</option>
<option value="MEDECINE DE CATASTROPHE">MEDECINE DE CATASTROPHE</option>
<option value="SANTE PUBLIQUE ET MEDECINE SOCIALE">SANTE PUBLIQUE ET MEDECINE SOCIALE</option>
<option value="SCIENCES ET HUMANITES">SCIENCES ET HUMANITES</option>
<option value="BIOTECHNOLOGIES">BIOTECHNOLOGIES</option>

<option value="INFORMATIQUE">INFORMATIQUE</option>
<option value="BIOTECHNOLOGIES">BIOTECHNOLOGIES</option>
<option value="ELECTRICITE ET ELECTRONIQUE">ELECTRICITE ET ELECTRONIQUE</option>
<option value="GENIE CHIMIQUE - GENIE DES PROCEDES">GENIE CHIMIQUE - GENIE DES PROCEDES</option>
<option value="TECHNIQUES DE COMMERCIALISATION">TECHNIQUES DE COMMERCIALISATION</option>

 </select>




 <select name="rechercheetablissement" id='rechercheetablissement'>

 <option value=''>-- Nom de l'etablissement--</option>
 <option value='Aix-Marseille Université'>Aix-Marseille Université </option>
 <option value='Avignon Université'>Avignon Université </option>
 <option value='Institut national universitaire Jean-François Champollion'>Institut national universitaire Jean-François Champollion </option>
 <option value='Le Mans Université'>Le Mans Université </option>
 <option value='Sorbonne Université'> Sorbonne Université  </option>
 <option value='Université Bordeaux-Montaigne'> Université Bordeaux-Montaigne  </option>
 <option value='Université Clermont Auvergne'> Université Clermont Auvergne </option>
<option value="Université d'Angers"> Université d'Angers  </option>
<option value="Université d'Artois"> Université d'Artois  </option>
<option value="Université d'Orléans"> Université d'Orléans </option>
<option value="Université d'Évry-Val d'Essonne">Université d'Évry-Val d'Essonne   </option>
<option value="Grenoble INP">Grenoble INP</option>
<option value="Institut national des sciences appliquées de Rennes">Institut national des sciences appliquées de Rennes</option>    
<option value="Institut national polytechnique de Toulouse">Institut national polytechnique de Toulouse</option>
<option value="Université Bourgogne - Franche-Comté">Université Bourgogne - Franche-Comté</option>
<option value="Université de Bourgogne">Université de Bourgogne</option>     
<option value="Université Bourgogne - Franche-Comté">Université Bourgogne - Franche-Comté</option>
 <option value="Université de Bretagne Occidentale">Université de Bretagne Occidentale</option>
<option value="Université de Caen Normandie">Université de Caen Normandie</option>
<option value="Université de Franche-Comté">Université de Franche-Comté</option>
<option value="Université de la Nouvelle-Calédonie">Université de la Nouvelle-Calédonie</option>

<option value="Université de La Rochelle">Université de La Rochelle</option>

<option value="Université de Limoges">Université de Limoges</option>
<option value="Université de Rennes 1">Université de Rennes 1</option>
<option value="Université de technologie de Troyes">Université de technologie de Troyes</option>
<option value="Université de Tours">Université de Tours</option>

<option value="Université Montpellier 3 - Paul-Valéry">Université Montpellier 3 - Paul-Valéry</option>
<option value="Université Jean Monnet">Université Jean Monnet</option>




 </select>







 <input type="submit" value="Rechercher">

</form>



    <table id='tab' class='display'>

        <caption>Etudes</caption>

        <thead>

            <tr>
            	
            <th>Etablissement</th>
            <th>Type de diplome</th>
             <th>Discipline</th>
             <th>Nom du diplome</th>
              <th>Ville</th>
            </tr>

        </thead>

        <tbody>
        	<?php 
        	foreach ($schools->records as $school) {
        	 ?>

            <tr>
            	<td><?php echo $school->fields->etablissement_lib; ?> </td>
    			
				<td><?php echo $school->fields->diplome_lib; ?> </td>
				<td><?php echo $school->fields->discipline_lib; ?> </td>
				<td><?php echo $school->fields->libelle_intitule_1; ?> </td>
				<td><?php echo $school->fields->com_etab_lib; ?> </td>

            </tr>



        <?php } ?>


        </tbody>

    </table>









<div class="center-div"></div>



<div id="searchbar">
               
                        <h1>Recherche sur l'enseignement supérieur</h1>
            






</div>







    <br>


 <div id="mapid"> </div>

<script type="text/javascript">

        var mymap = L.map('mapid').setView([46.837189, 0.584856], 6);

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/streets-v11'
    }).addTo(mymap);
//var marker = L.marker([50.00000, 2.44239]).addTo(mymap);

<?php

	$etab = array();
    $etabX = array();
    $etabY = array();



    foreach ($school2['records'] as $value) {

    	foreach ($etablissement['records'] as $value2) {
            if (isset($value2['fields']['coordonnees']) && isset($value2['fields']['uai']) && ($value['fields']['etablissement'] == $value2['fields']['uai']) && in_array($value2['fields']['uai'],$etab) == FALSE ) {
            	//echo $value2['fields']['uai'] . " " . $value['fields']['etablissement'] . "\n";
            	$etab[] = $value2['fields']['uai'];
                $etabX[] = $value2['fields']['coordonnees'][0];
                $etabY[] = $value2['fields']['coordonnees'][1];
            }
        }
    }

/*
$toto = array();
    for($i = 0; $i<sizeof($etab);$i++){
       echo "L.marker([" . $etabX[$i] . ", " . $etabY[$i] . "]).addTo(mymap);\n"; 



 }
*/
$toto = array();
    for($i = 0; $i<sizeof($etab);$i++){
       echo "L.marker([" . $etabX[$i] . ", " . $etabY[$i] . "]).bindPopup(\"<a href='https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&rows=-1'>Site</a>\").addTo(mymap);\n"; 
 }




?>

</script>



    </body>
</html>

